var mongoose = require('mongoose');

var RecordSchema = mongoose.Schema({
    hostName: String,
    challengerName: String,
    hostScore: Number,
    challengerScore: Number,
    duelReason: String,
    date: {type: Date, default: Date.now }
});

mongoose.model('Record', RecordSchema);
module.exports = mongoose.model('Record');