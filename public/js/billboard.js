Billboard = function( geometry, material, engine ){
    THREE.Mesh.call( this, geometry, material );
    this.engine = engine;
};

Billboard.prototype = Object.create( THREE.Mesh.prototype );

Billboard.prototype.updateMatrix = function(){
    //This is called every frame by the renderer
    var cam = this.engine.controls.getObject();

    //Set the object's orintation to face the camera
    var current_y = this.rotation.y;
    this.rotation.y = 1.52-Math.atan2(
        cam.position.z - this.position.z,
        cam.position.x - this.position.x
    );
    if(this._shadowMesh){
        this._shadowMesh.position.x = this.position.x;
        this._shadowMesh.position.z = this.position.z;
    }

    THREE.Mesh.prototype.updateMatrix.call(this);

    //save modified rotation for point determinance
    this.billboardRotation = this.rotation.y;
    //Reset rotation, so it can be used for other purposes.
    this.rotation.y = current_y;
};

Billboard.prototype.addShadow = function(size, engine) {
    this.engine = engine;
    this._shadowMesh = new THREE.Mesh(new THREE.PlaneGeometry(size,size,4,4), new THREE.MeshBasicMaterial( { map: this.engine.textures.shadow, transparent: true, depthWrite: false, depthTest: false } ));
    this._shadowMesh.rotation.x = d2r(-90);
    engine.scene.add(this._shadowMesh);
};

Billboard.prototype.getLocalPoint = function(globalPoint){
    var me = this;
    var nX = globalPoint.x - me.position.x;
    var nY = me.geometry.height - globalPoint.y;// - me.position.y;
    var nZ = globalPoint.z - me.position.z;

    var rX = nX / Math.cos(me.billboardRotation);

    return {
        x: rX + me.geometry.width * 0.5,
        y: nY
    };
};

Billboard.prototype.updateMatrix = function(){
    //This is called every frame by the renderer
    var cam = this.engine.controls.getObject();

    //Set the object's orintation to face the camera
    var current_y = this.rotation.y;
    this.rotation.y = 1.52-Math.atan2(
        cam.position.z - this.position.z,
        cam.position.x - this.position.x
    );
    if(this._shadowMesh){
        this._shadowMesh.position.x = this.position.x;
        this._shadowMesh.position.z = this.position.z;
    }

    THREE.Mesh.prototype.updateMatrix.call(this);

    //save modified rotation for point determinance
    this.billboardRotation = this.rotation.y;
    //Reset rotation, so it can be used for other purposes.
    this.rotation.y = current_y;
};


var Gib = function(tex, engine) {
	console.log('gib texture',engine.textures[tex]);
	Billboard.call(this, new THREE.PlaneGeometry(engine.textures[tex].image.width, engine.textures[tex].image.height, 10, 10), new engine.baseMaterial( { map: engine.textures[tex], transparent: true, depthWrite: false, depthTest: true } ));
	this.texture = tex;
	this.engine = engine;

	this.vely = this.velx = this.velz = 0;
	this.gravity = 0;
	this.rotspeed = 0;

	this.visible = false;

};
Gib.prototype = Object.create( Billboard.prototype );

Gib.prototype.explode = function() {

	this.visible = true;

	this.gravity = 0.2;

	this.rotspeed = (Math.random() * -0.1) + 0.1;
	this.velx = rndInt(-2,2);
	this.vely = rndInt(1,3);
	this.velz = rndInt(-2,2);

};

Gib.prototype.hide = function() {
	this.vely = this.velx = this.velz = 0;
	this.gravity = 0;
	this.rotspeed = 0;

	this.visible = false;
};

Gib.prototype.updateMatrix = function() {

     if(!this.visible) return;

	 Billboard.prototype.updateMatrix.call(this);

	 if(this.gravity == 0) return;

	 this.rotation.z += this.rotspeed;
	 this.position.x += this.velx;
	 this.position.y += this.vely;
	 this.position.z += this.velz;

	 this.vely -= this.gravity;

	 if(this.position.y <= 0) {
	 	 this.position.y = 0;
	 	 this.vely = -(this.vely / 2);
	 	 if(Math.abs(this.vely) < 1.5) {
			 this.vely = this.velx = this.velz = 0;
			 this.gravity = 0;
			 this.rotspeed = 0;
		 }
	 }

};
