var UI = function() {
	var me = this;

	me.elements = {
		deathFade: $('#overlay-red')
	}


};

var Engine = function(callback){
    var me = this;

    me.messages = [];

    me.ui = new UI();

    me.sounds = {
        'ui-hover' : 'ui-hover',
        'ui-click' : 'ui-click',
        'fire' : 'fire',
        'fire2' : 'fire',
        'gun-cock' : 'gun-cock',
        'gore' : 'gore',
        'vox-hype' : 'vox-hype',
        'footstep' : 'footstep',
        'mus-title' : 'mus-title',
        'mus-endround' : 'mus-endround'
    };
    me.loadSounds();

    me.textures = {
        grass: 'textures/grass.jpg',
        player: 'sprites/anims/shooter/shooter_walk_a_1.png',
        tree1: 'sprites/tree1.png',
        tree2: 'sprites/tree2.png',
        shadow: 'textures/shadow.png',
        hedge: 'textures/hedge.png',
        house: 'sprites/house.png',
        bang: 'sprites/placeholder_bang.png',
        bloodstain: 'textures/bloodstain.png'
    };
    for(var i = 0; i < 8; i++) {
    	if(i < 4) me.textures['shooter_bloodsplat_' + (i+1)] = 'sprites/anims/bloodsplat/bloodsplat_' + (i+1) + '.png';

    	me.textures['gib_' + (i+1)] = 'sprites/anims/gibs/gib_' + (i+1) + '.png';
    }

    me.skytex = {
        sky_px: 'img/textures/sky1/posx.jpg',
        sky_py: 'img/textures/sky1/posy.jpg',
        sky_pz: 'img/textures/sky1/posz.jpg',
        sky_nx: 'img/textures/sky1/negx.jpg',
        sky_ny: 'img/textures/sky1/negy.jpg',
        sky_nz: 'img/textures/sky1/negz.jpg'
	};

	var urls = [
        me.skytex.sky_px, me.skytex.sky_nx,
        me.skytex.sky_py, me.skytex.sky_ny,
        me.skytex.sky_pz, me.skytex.sky_nz
    ];

    me.skycube = THREE.ImageUtils.loadTextureCube( urls );
    me.skycube.format = THREE.RGBFormat;

    me.initialise();

    var pfx = enemy_anims.prefix;
    var angles = ['a','b','c','d','e'];
    for (var k in enemy_anims.anims) {
    	var anim = enemy_anims.anims[k];
    	for (var l=0; l< anim.frames; l++) {
    	    if(anim.noangles !== true) {
                for (var j=0; j<angles.length; j++) {

                    var skey = pfx + '_' + anim.sprite + '_' + angles[j] + '_' + (l+1);
                    me.textures[skey] = 'sprites/anims/shooter/' + skey + '.png';

                }
            } else {

                var skey = pfx + '_' + anim.sprite + '_' + (l+1);
                me.textures[skey] = 'sprites/anims/shooter/' + skey + '.png';
            }
    	}
    }

    me.loadTextures(me.textures, function() { me.prepSpriteRotations(callback) });
};

Engine.prototype.loadSounds = function() {
    var me = this;

    for (var k in me.sounds) {

        me.sounds[k] = new Audio('audio/' + me.sounds[k] + '.mp3');
        me.sounds[k].load();
        console.log('load sound', me.sounds[k]);
    }

    me.sounds['mus-title'].volume = 0.6;
    me.sounds['mus-title'].loop = true;

};

Engine.prototype.playSound = function(id) {

    console.log('playSound', id);

    var me = this;

    if(!me.options.sound) return;

    //me.sounds[id].stop();

    if(id.substr(0,3) == 'mus') {
        if (me.options.musvol == 0) return;
        me.sounds[id].volume = me.options.musvol;
    }

    me.sounds[id].currentTime = 0;
    me.sounds[id].play();

};

Engine.prototype.resizeHandler = function() {

    var me = this;
    console.log('resizeHandler', me);

    me.renderer.setSize( window.innerWidth, window.innerHeight );
    me.camera.aspect	= window.innerWidth / window.innerHeight;
    me.camera.updateProjectionMatrix();

    me.hudCanvas.height = window.innerHeight;
    me.hudCanvas.width = window.innerWidth;

};

Engine.prototype.initialise = function(){
    var me = this;
    me.scene = null;
    me.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 3000);

    me.stereo_sep = -0.04800000000000005;    // distance between 2 cameras for stereoscopy

    me.renderer = new THREE.WebGLRenderer({ antialias: false });
    me.renderer.autoClear = false;
    me.renderer.setSize( window.innerWidth, window.innerHeight );
    me.projector = new THREE.Projector();
    me.hudCanvas = document.createElement('canvas');
    $(me.hudCanvas).addClass('hud-canvas');
    me.hudCanvas.height = window.innerHeight;
    me.hudCanvas.width = window.innerWidth;
    me.hud = me.hudCanvas.getContext('2d');
    me.bob = 0.2;
    me.footstepCount = 0;

    me.debouncer = null;

    $(window).on('resize', function() {
            clearTimeout(me.debouncer);
            me.debouncer = setTimeout(function() {
                    me.resizeHandler.call(me);
            }, 400);
    });

}

Engine.prototype.loadTextures = function(textures, callback){
    var ct = init_ct = Object.keys(textures).length;
    var progress = $('#loading progress');
    console.log('Loading ', ct, ' textures');
    for (var k in textures) {
        var fn = 'img/' + textures[k];

        textures[k] = THREE.ImageUtils.loadTexture(fn, {}, function() {
            ct--;
            var pc = 100 - Math.floor((ct / init_ct) * 100);
            progress.attr('value', pc);
            console.log(ct, ' textures remain - ', pc + '%');
            if (ct == 0) {
                console.log('all textures loaded');
                callback();
            }
        });
        textures[k].minFilter = THREE.NearestFilter;
    	textures[k].magFilter = THREE.NearestFilter;

    }
    textures.grass.wrapS = textures.grass.wrapT = THREE.RepeatWrapping;
    textures.grass.repeat.set( 8, 8 );

    textures.hedge.wrapS = textures.hedge.wrapT = THREE.RepeatWrapping;
    textures.hedge.repeat.set( 20, 2 );
};

Engine.prototype.prepSpriteRotations = function(callback) {
    console.log('prepSpriteRotations');
    var me = this;

    var pfx = enemy_anims.prefix;
    var flip_map = [['f','d'],['g','c'],['h','b']];
    for (var k in enemy_anims.anims) {
    	var anim = enemy_anims.anims[k];

    	if(anim.noangles === true) continue;

    	for (var j=0; j < flip_map.length; j++) {
    		for (var l=0; l< anim.frames; l++) {
    		    var canvas = document.createElement("canvas");
                canvas.width = canvas.height = 180;
                var context = canvas.getContext("2d");
                context.translate(canvas.width, 0);
                context.scale(-1, 1);

    			var flip_key = pfx + '_' + anim.sprite + '_' + flip_map[j][0] + '_' + (l+1);
    			var orig_key = pfx + '_' + anim.sprite + '_' + flip_map[j][1] + '_' + (l+1);

    			var original = me.textures[orig_key].image;

    			context.clearRect(0, 0, canvas.width, canvas.height);
    			context.drawImage(original, 0, 0);

    			me.textures[flip_key] = new THREE.Texture(canvas);
    			me.textures[flip_key].minFilter = THREE.NearestFilter;
    			me.textures[flip_key].magFilter = THREE.NearestFilter;
    			me.textures[flip_key].needsUpdate = true;

    		}
    	}
    }

    //me.debugTextures();
    callback();
};

Engine.prototype.debugTextures = function() {
    $('.content').html('');
    $('body').css('background','#111');
    var me = this;
    console.log('all textures', me.textures);
    for(var k in me.textures) {
        var item = me.textures[k].image;
        var src = (item.nodeName == 'CANVAS') ? item.toDataURL('image/png') : item.attributes.src.value;
        var col = (item.nodeName == 'CANVAS') ? 'red' : 'blue';
        $('body').prepend('<div style="position:relative;display:inline-block;border:1px solid ' + col + '; margin:10px;"><img style="margin:0;" src="' + src + '" /><small style="position:absolute;bottom:0;right:0;">' + k + '</small></div>');
    }
};

Engine.prototype.start = function(messenger, sharedState){
    var me = this;

    if(me.options.stereoscopy) {

        me.camera2 = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 3000);

        $('#hud, #gun').hide();

    } else {
        me.camera2 = null;
    }

    me.deltaTimer = Date.now();
    me.messenger = messenger;
    me.sharedState = sharedState;
    me.wonLast = false;

    me.reloader = {
        element: $('#reloader'),
        gauge: $('#reloader progress'),
        keys: [113,112],    // q,p
        alternator: 0,
        currentLevel: 0,
        decayCounter: 0
    };

    me.resetStatus();
    me.setupGameScene();
    me.setupEnemy();
    me.bindEvents();
    me.render();

    if(me.messenger.socket){
        me.messenger.socket.on('walk', me.walkPhase.bind(me));
        me.messenger.socket.on('fire', me.firePhase.bind(me));
        me.messenger.socket.on('outcome', me.handleOutcome.bind(me));
        me.messenger.socket.on('finish', me.handleGameover.bind(me));
    }

    if (me.sharedState){
        me.messenger.socket.on('roundStart', me.onRoundStart.bind(me));
        me.sharedState.addRefreshListener(me.updateHUD.bind(me));
        me.sharedState.refresh();
        setTimeout(function(){ me.sceneTransition(function(){me.messenger.socket.emit('reset');}); }, 10000)
        me.hudMessage(me.sharedState.get('hostName') + ": \"" + me.sharedState.get('duelReason') + "\"", 4000);

        var responses = [
            "How very dare you, sir!",
            "I'll have your head for that!",
            "This bounder has no honour!",
            "I've left more honour in the jakes.",
            "God's Blood, I shall see you fall."
        ];

        var retort = responses[Math.floor(Math.random()*responses.length)];
        setTimeout(function(){
            me.hudMessage(me.sharedState.get('challengerName') + ": \"How very dare you, sir!\"", 4000);
        }, 5000);
    } else {
        me.allowedToWalk = true;
        me.shootTimer = 0;
    }

    me.controls.clampPitch(DEG_22_5);
};

Engine.prototype.resetStatus = function() {

    console.log('resetStatus');

    var me = this;

    me.chargeLevel = 1;

    me.ammoCount = 1;
    console.log('ammo',me.ammoCount);
    $('#ammo span').html(me.ammoCount);
    $('#ammo').show();
    me.reloader.element.hide();
    if(me.hudweapon)
        me.hudweapon.removeClass('reload');

    me.reloader.strength = 2;
    me.reloader.decayTic = 10;
    me.reloader.max = 50;

    // TODO - handle modifiers here


};

Engine.prototype.onRoundStart = function(){
    var me = this;
    me.resetStatus();

    me.opponent.logic.exploding = false;
    me.opponent.sprite.visible = true;
	me.opponent.sprite._shadowMesh.visible = true;
	me.opponent.hasExploded = false;
	me.removeGibs();

    me.bloodView = false;
    me.controlsObject.position.x = Math.random() * 80 - 40;
    me.controlsObject.position.z = me.messenger.participant == 0 ? -30: 30;
    me.controlsObject.position.z += Math.random() * 50 - 25;
    me.controlsObject.rotation.y = me.messenger.participant == 0 ? 0: Math.PI;
    me.controlsObject.rotation.y += Math.random() * DEG_45 - DEG_22_5;
    me.messenger.socket.on('countdown', me.doCountdown.bind(me));
    me.hudMessage('Round ' + me.sharedState.get('currentRound'), 2000);

    var name = me.sharedState.get('hostName');
    if (
        (me.wonLast && me.messenger.participant === 0) ||
        (!me.wonLast && me.messenger.participant === 1)
    ){
        name = me.sharedState.get('challengerName');
    }
    setTimeout(function(){
        me.hudMessage(name + ": \"" + generateInsult() + "\"", 3000);
    }, 2500);

    $('#current-round').html(me.sharedState.get('currentRound'));
    $('#score-1').html(me.sharedState.get('hostScore'));
    $('#score-2').html(me.sharedState.get('challengerScore'));
}

Engine.prototype.updateHUD = function(){
    var me = this;
    $('#hud-name-1 .name').html(me.sharedState.get('hostName'));
    $('#hud-name-2 .name').html(me.sharedState.get('challengerName'));
    $('#current-round').html(me.sharedState.get('currentRound'));
    $('#score-1').html(me.sharedState.get('hostScore'));
    $('#score-2').html(me.sharedState.get('challengerScore'));
}

Engine.prototype.doCountdown = function(count){
    var me = this;
    me.messenger.socket.removeAllListeners('countdown');
    me.countdown = count;
    !function countdown(){
        if(me.countdown > 0){
            setTimeout(countdown, 1000);
            if(me.countdown <= 5)
                me.hudMessage(me.countdown, 400);
            me.countdown--;
        }
    }();
}

Engine.prototype.walkPhase = function(){
    var me = this;
    me.hudMessage('Walk!', 6000);
    me.allowedToWalk = true;
    me.controls.speedModifier = 0.25;
    me.controls.clampYaw(DEG_22_5);
    me.controls.setForwardOnly(true);
};

Engine.prototype.firePhase = function(){
    var me = this;
    me.shootTimer = 0;
    me.hudMessage('Turn and fire!', 3000);
    me.controls.speedModifier = 1;
    me.controls.unclampYaw();
    me.controls.setForwardOnly(false);
};

Engine.prototype.startReload = function() {
    var me = this;
    console.log('start reload');
    $('#ammo').hide();
    me.reloader.element.show();
    me.reloader.currentLevel = 0;
    me.reloader.gauge.attr('value', me.reloader.currentLevel);
    me.reloader.alternator = 0;

    me.hudweapon.addClass('reload');

    $(document).on('keypress.reload', function(e) {

            if(e.which == me.reloader.keys[me.reloader.alternator]) {
                me.reloader.currentLevel += me.reloader.strength;
                me.reloader.alternator ^= 1;
            }
    });
};

Engine.prototype.checkReload = function() {

    var me = this;

    me.reloader.gauge.attr('value', Math.min((me.reloader.currentLevel / me.reloader.max) * 100, 100));

    if(me.reloader.currentLevel >= me.reloader.max) me.doReload();

    me.reloader.decayCounter++;
    if(me.reloader.decayCounter >= me.reloader.decayTic) {
        if(me.reloader.currentLevel > 0) me.reloader.currentLevel--;
        me.reloader.decayCounter = 0;
    }

};

Engine.prototype.doReload = function() {

    var me = this;
    console.log('do reload');
    $(document).off('keypress.reload');

    me.ammoCount++;

    $('#ammo span').html(me.ammoCount);
    $('#ammo').show();
    me.reloader.element.hide();
    me.reloader.currentLevel = 0;

    me.shootTimer = 0;

    me.hudweapon.removeClass('reload');

};

Engine.prototype.deathAnim = function(){
    var me = this;
    me.sounds.fire2.volume = 0.5;
    me.playSound('fire2');
    me.playSound('gore');
    me.bloodView = true;
    me.addBloodstain(me.controlsObject.position.x, me.controlsObject.position.z);
    setTimeout(function() {
	        me.playSound('mus-endround');
	}, 2000);
};

Engine.prototype.handleOutcome = function(data){
    var me = this;
    function prepReset(){
        if(me.sharedState.get('currentRound') <= me.sharedState.get('numRounds')){
            me.sceneTransition(function(){ me.messenger.socket.emit('reset'); });
        }
    }
    switch(data.win){
        case "win":
            me.hudMessage('You win this round', 3000);
            me.allowedToWalk = false;
            me.wonLast = true;
            setTimeout(prepReset, 5000);
            break;
        case "lose":
            me.deathAnim();
            me.allowedToWalk = false;
            me.shootTimer = 1;
            me.wonLast = false;
            setTimeout(prepReset, 5000);
            break;
        case "draw":
            me.deathAnim();
            me.allowedToWalk = false;
            me.shootTimer = 1;
            me.wonLast = false;
            setTimeout(prepReset, 5000);
            break;
        case "none":
            me.hudMessage('Reload!', 2000);
            me.shootTimer = 0; // TEMPORARY
            break;
    }
}

Engine.prototype.handleGameover = function(data){
    var me = this;
    if (data.winner == me.messenger.participant){
        me.bloodView = false;
        me.hudMessage('Game Over - Winner!');
        me.allowedToWalk = true;
    } else if (data.winner == 2){
        me.bloodView = false;
        me.hudMessage('Game Over - Draw!');
        me.allowedToWalk = true;
    } else {
        me.hudMessage('Game Over - Loser!');
    }
    setTimeout(function(){ window.location.reload();}, 10000);
}

Engine.prototype.stop = function()
{
    var me = this;
    me.teardownGameScene();
    me.unbindEvents();
};

Engine.prototype.setupGameScene = function(){
    var me = this;
    me.scene = new THREE.Scene();
    me.fxScene = new THREE.Scene();
    me.deepScene = new THREE.Scene();
    me.scene.fog = new THREE.Fog(0x666666, 100, 1000);
    me.ambient = new THREE.AmbientLight(0xaaaaaa);
    me.scene.add(me.ambient);
    me.fxScene.add(new THREE.AmbientLight(0xffffff));
    me.deepScene.add(new THREE.AmbientLight(0xffffff));
    me.deepScene.fog = new THREE.Fog(0x666666, 250, 2500);

    me.boundaryRadius = 500;

    me.baseMaterial = (typeof me.ambient != 'undefined') ? THREE.MeshLambertMaterial : THREE.MeshBasicMaterial;

    me.entities = {};

    // skybox
    var shader = THREE.ShaderLib[ "cube" ];
    shader.uniforms[ "tCube" ].value = me.skycube;

    var skymat = new THREE.ShaderMaterial( {

        fragmentShader: shader.fragmentShader,
        vertexShader: shader.vertexShader,
        uniforms: shader.uniforms,
        depthWrite: false,
        side: THREE.BackSide

    } );

    me.entities.skymesh = new THREE.Mesh( new THREE.CubeGeometry( 3000, 3000, 3000, 1, 1, 1, null, true ), skymat );
    me.deepScene.add( me.entities.skymesh );

    me.entities.floor = new THREE.Mesh( new THREE.PlaneGeometry(2000,2000,10,10) , new me.baseMaterial( { map: me.textures.grass } ) );
    me.entities.floor.rotation.x = d2r(-90);
    me.scene.add(me.entities.floor);

    // house
    var house = new THREE.Mesh(
        new THREE.PlaneGeometry(4000, 2144, 10, 10),
        new me.baseMaterial( { map: me.textures.house, transparent: true, depthWrite: true, depthTest: true })
    );
    house.rotation.y = DEG_90;
    house.position.y = 850;
    house.position.x = -2000;
    me.deepScene.add(house);

    me.addHedges(me.boundaryRadius + 100);
    me.addTrees(me.boundaryRadius, 10);

    me.addGibs();

    me.camera.position.y = 60;
    if(me.options.stereoscopy) me.camera2.position.y = 60;
    me.controls = new THREE.PointerLockControls( me.camera, me.camera2 );
    me.allowedToWalk = false;
    me.playerState = new StateTracker(me);
    me.shootTimer = 1;

    me.controlsObject = me.controls.getObject();
    me.controlsObject.position.z = me.messenger.participant == 0 ? -40: 40;
    me.controlsObject.rotation.y = me.messenger.participant == 0 ? Math.PI: 0;
    me.scene.add(me.controlsObject);
    me.maincanvas = me.renderer.domElement;
    $('#maincanvas').append(me.maincanvas);
    $('body').append(me.hudCanvas);

    me.hudweapon = $('#gun');
};

Engine.prototype.addGibs = function(entity) {

	var me = this;
	me.gibs = [];
	for(var i = 0; i < 8; i++) {
		me.gibs[i] = new Gib('gib_' + (i+1), me);
		me.scene.add(me.gibs[i]);
	}

};

Engine.prototype.spawnGibs = function(entity) {
	console.log('spawnGibs');
	var me = this;
	for(var i = 0; i < me.gibs.length; i++) {
		me.gibs[i].position.set(entity.position.x + rndInt(-30, 30), 60 + rndInt(-30, 30), entity.position.z + rndInt(-30, 30));
		me.gibs[i].explode();
	}

};

Engine.prototype.removeGibs = function() {

	var me = this;
	for(var i = 0; i < me.gibs.length; i++) {
		me.gibs[i].hide();
	}

};

Engine.prototype.teardownGameScene = function(){
    $('#maincanvas').html();
};

Engine.prototype.addHedges = function(spacing){
    var me = this;
    var walls = [
        [0, spacing],
        [spacing, 0],
        [0, -spacing],
        [-spacing, 0]
    ];
    for(var i = 0; i < walls.length; i++) {
        var pos = walls[i];
        var hedge = new THREE.Mesh( new THREE.PlaneGeometry(1200,120, 10,10) , new me.baseMaterial( { map: me.textures.hedge, side: THREE.BackSide } ) );
        hedge.position.set(pos[0], 60, pos[1]);
        var rot = d2r(90 * i);
        hedge.rotation.y = rot;
        console.log('hedge ', i , hedge);
        me.scene.add( hedge );
    }
};

Engine.prototype.addTrees = function(radius, step){
    var me = this;
    var merged;
    var ang = 0;
    while( ang < 360 ) {

        var tree;
        if (rndInt(0,10) > 5) {
            var tree = new Billboard( new THREE.PlaneGeometry(83, 226, 10, 10), new me.baseMaterial( { map: me.textures.tree1, transparent: true, depthWrite: false, depthTest: true } ) );
            tree.position.y = 113;
        } else {
            var tree = new Billboard( new THREE.PlaneGeometry(120, 217, 10, 10), new me.baseMaterial( { map: me.textures.tree2, transparent: true, depthWrite: false, depthTest: true } ) );
            tree.position.y = 108;
        }

        tree.position.x = radius * Math.cos(ang);

        tree.position.z = radius * Math.sin(ang);
        me.scene.add(tree);
        tree.addShadow(200, me);

        ang += step;
    }
    //me.scene.add(merged);
};

Engine.prototype.setupEnemy = function(){
    var me = this;
    me.opponent = new Enemy({engine: me});
    me.messenger.subject = me.opponent;
    me.opponent.sprite.position.z = me.messenger.participant == 0 ? 40 : -40;
    me.opponent.sprite.rotation.y = me.messenger.participant == 0 ? 0: Math.PI;
};

Engine.prototype.hudMessage = function(text, duration){
    var me = this;
    me.messages.push(new HUDMessage(text, duration));
};

Engine.prototype.footstep = function() {

	var me = this;
	if(me.controls.getVelocity() > 0.1) {
		me.footstepCount += me.controls.getVelocity();
		if(me.footstepCount > 20) {
			me.playSound('footstep');
			me.footstepCount = 0;
		}
	}


};

Engine.prototype.renderToCameras = function(scene) {

    var me = this;

    if(me.options.stereoscopy) {

        var width = Math.round(window.innerWidth/2),
        height = window.innerHeight;

        // Render the scene
        me.renderer.setViewport( 0, 0, width, height);
        me.renderer.setScissor( 0, 0, width, height);
        me.renderer.enableScissorTest ( true );

        me.camera.aspect = width * 2 / height;
        me.camera.updateProjectionMatrix();
        me.camera.position.x = me.stereo_sep;

        me.renderer.render( scene, me.camera );

        me.renderer.setViewport( width, 0, width, height);
        me.renderer.setScissor( width, 0, width, height);
        me.renderer.enableScissorTest ( true );

        me.camera2.aspect = width * 2 / height;
        me.camera2.updateProjectionMatrix();
        me.camera2.position.x = -me.stereo_sep;

        me.renderer.render( scene, me.camera2 );


    } else {

        me.renderer.render( scene, me.camera );

    }

};

Engine.prototype.render = function(timestamp)
{
    var me = this;
    var step = timestamp - me.deltaTimer;
    me.deltaTimer = timestamp;
    requestAnimationFrame(me.render.bind(me));

    me.entities.skymesh.position = me.controlsObject.position;

    if(me.allowedToWalk && me.mouseInGame)
        me.controls.enabled = true;
    else
        me.controls.enabled = false;

    me.controls.update(step);

    me.updatePlayerState();
    me.updateEnemyState();

    if(me.ammoCount == 0) me.checkReload();

    if(me.playerState.getState() == "moving" && !me.charging){
        if (me.camera.position.y > 62|| me.camera.position.y < 58)
        {
            me.bob *= -1;
        }
        me.camera.position.y += me.bob;
        if(me.options.stereoscopy) me.camera2.position.y = 60;
    }

    me.footstep();

    me.renderer.clear();
    me.renderToCameras( me.deepScene );
    me.renderer.clear( false, true, false );
    me.renderToCameras( me.scene );
    me.renderer.clear( false, true, false );
    me.renderToCameras( me.fxScene );

    // SHADER DONT LIKE ME - THEY CRASH EVERY BROWSER :D
    // var composer = new THREE.EffectComposer(me.renderer);
    // composer.addPass(new THREE.RenderPass(me.scene, me.camera));
    // var interfere = new THREE.ShaderPass( SHADERS.interfere );
    // interfere.renderToScreen = true;
    // interfere.uniforms[ "lines" ].value = 100;
    // interfere.uniforms[ "amount" ].value = 0.1; // damages = how many damage the tank get the last second
    // interfere.uniforms["p"].value = Math.sin(step/15);
    // interfere.uniforms[ "seed" ].value = Math.random();
    // composer.addPass(interfere);
    // composer.render();

    var rotation = me.controlsObject.rotation.y - 2.356194489;

    me.messenger.update({
        x: me.controlsObject.position.x,
        y: me.controlsObject.position.y,
        z: me.controlsObject.position.z,
        rotation: rotation,
        state: me.playerState.getState()
    });

    var sway;
    var coef = 10;
    if (me.charging){
        me.chargeLevel -= 0.00015 * step;
        if(me.swaying){
            sway = 50;
            coef *= me.chargeLevel;
            sway += (Math.random() * coef) - coef/2;
            me.hudweapon.css({
                left: sway + "%"
            });
        }
        if (me.chargeLevel < 0.1)
        {
            me.chargeLevel = 0.1;
            me.misfire();
        }
    } else {
        me.hudweapon.css({
            left: ""
        });
        me.swaying = false;
        me.chargeLevel += 0.02;
        if (me.chargeLevel > 1)
            me.chargeLevel = 1;
    }

    me.hud.clearRect(0, 0, me.hudCanvas.width, me.hudCanvas.height);
    if(!me.shootTimer) {
        me.hud.strokeStyle = "#CCCCCC";
        me.hud.beginPath();
        me.hud.arc(me.hudCanvas.width/2, me.hudCanvas.height/2, 50 * me.chargeLevel, 0, 2*Math.PI);
        me.hud.stroke();
        me.hud.beginPath();
        me.hud.arc(me.hudCanvas.width/2, me.hudCanvas.height/2, 2, 0, 2*Math.PI);
        me.hud.stroke();
    }

    for (var idx = 0; idx < me.messages.length; idx++){
        me.messages[idx].render(step, me.hudCanvas, 70 + (50 * idx));
        if(me.messages[idx].alpha < 0){
            me.messages.splice(idx, 1);
        }
    }

    if(me.bloodView){
        me.hud.fillStyle = "rgba(200, 0, 0, 0.6)";
        me.hud.fillRect(0, 0, me.hudCanvas.width, me.hudCanvas.height);
    }

    if (me.transition){
        me.transition.render(step, me.hudCanvas);
        if (me.transition.done) {
            delete me.transition;
        }
    }
    //console.log(me.playerState.state);
};

Engine.prototype.sceneTransition = function(callback, coef) {
    var me = this;
    me.transition = new SceneTransition(callback, coef);
};

Engine.prototype.updatePlayerState = function() {

	var me = this;

	if (me.charging) {
		me.playerState.setState('shooting');
	} else if (me.controls.isMoving()) {
		me.playerState.setState('moving');
	} else {
		me.playerState.setState('stationary');
	}

	var mypos = me.controlsObject.position;
	if (Math.sqrt(Math.pow(-mypos.x, 2) + Math.pow(-mypos.z, 2)) > me.boundaryRadius - 50) {
	    var radians = Math.atan2(mypos.z, mypos.x);
        mypos.x = Math.cos(radians) * (me.boundaryRadius - 50);
        mypos.z = Math.sin(radians) * (me.boundaryRadius - 50);
	}

};

Engine.prototype.updateEnemyState = function() {

	var me = this;

	// TODO: update enemy rotation and state from the message

	me.opponent.animate();

};

Engine.prototype.bindEvents = function(){
    var me = this;
    var bod = document.body;
    $('body').on('click', function() {

        if (me.controls.enabled == false) {
            bod.requestPointerLock = bod.requestPointerLock || bod.mozRequestPointerLock || bod.webkitRequestPointerLock;
            bod.requestPointerLock();
        }

    });

    var onPointerLockChange = function(event) {
        console.log('pointer lock change');
        if ( document.pointerLockElement === bod || document.mozPointerLockElement === bod || document.webkitPointerLockElement === bod ) {

            me.mouseInGame = true;
            $('#clickme').hide();

        } else {

            me.mouseInGame = false
            $('#clickme').show();

        }
    };

    document.addEventListener( 'pointerlockchange', onPointerLockChange, false );
    document.addEventListener( 'mozpointerlockchange', onPointerLockChange, false );
    document.addEventListener( 'webkitpointerlockchange', onPointerLockChange, false );


    $(document).on('mousedown', function(){
        if(me.shootTimer) return;
        me.hudweapon.addClass('aim');
        me.charging = true;
        me.controls.speedModifier = 0.25;
        me.playSound('gun-cock');
        setTimeout(function(){ me.swaying = true;}, 250);
    });

    $(document).on('mouseup', function(){
        if(me.shootTimer || me.bloodView) return;
        me.charging = false;
        me.controls.speedModifier = 1;
        me.shoot();
        setTimeout(function() {
                me.hudweapon.removeClass('aim');

                me.ammoCount--;
                $('#ammo span').html(me.ammoCount);
                console.log('ammo',me.ammoCount);
                if(me.ammoCount == 0) me.startReload();
        }, 250);
    });
};

Engine.prototype.unbindEvents = function(){

};

Engine.prototype.shoot = function(){
    var me = this;

    if (me.shootTimer || me.ammoCount == 0) return;

    var v = me.controls.getObject().position.clone();
    v.y = 70;
    var direction = me.controls.getDirection();
    var variance;
    var displace;
    if (Math.random() > 0.4){
        variance = 0.08 * me.chargeLevel;
        displace = 0;
    } else {
        variance = 0.02 * me.chargeLevel;
        displace = 0.06 * me.chargeLevel;
    }
    direction.y += displace + (Math.random() * variance) * (Math.random() > 0.5 ? -1 : 1);
    direction.x += displace + (Math.random() * variance) * (Math.random() > 0.5 ? -1 : 1);
    direction.z += displace + (Math.random() * variance) * (Math.random() > 0.5 ? -1 : 1);
    var raycaster = new THREE.Raycaster(v, direction);

    var intersects = raycaster.intersectObjects(me.scene.children);

    var collidee;
    for(var idx = 0; idx < intersects.length; idx++){
        if(intersects[idx].object instanceof Billboard){

            // relative offset checking not working for enemy atm.
            // So if enemy, just bail out

            var relativeOffset = intersects[idx].object.getLocalPoint(intersects[idx].point);

            if(intersects[idx].object.enemy){
                var model = intersects[idx].object.model;
                console.log("[" + model.hitWidth + ", " + model.hitHeight + "]");
                if (relativeOffset.x > 50 - model.hitWidth/2 &&
                    relativeOffset.x < 50 + model.hitWidth/2 &&
                    relativeOffset.y > 100 - model.hitHeight
                ){
                    collidee = intersects[idx];
                    break;
                } else {
                    continue;
                }
            }

            var map = intersects[idx].object.material.map;
            if(!map){
                console.log(intersect[idx]);
                continue;
            }

            var ctx = document.createElement('canvas').getContext('2d');
            ctx.drawImage(map.image, 0, 0);
            var alpha = ctx.getImageData(Math.round(relativeOffset.x), Math.round(relativeOffset.y), 1, 1).data[3];
            if (alpha > 0)
            {
                collidee = intersects[idx];
                break;
            }
        } else {
            collidee = intersects[idx];
            break;
        }
    }

    if (collidee) {
        console.log('intersect: ' + collidee.point.x.toFixed(2) + ', ' + collidee.point.y.toFixed(2) + ', ' + collidee.point.z.toFixed(2) + ')');
        var bang = new Billboard(
            new THREE.PlaneGeometry(10, 10, 10, 10),
            new me.baseMaterial({
                map: me.textures.bang,
                transparent: true,
                depthWrite: false,
                depthTest: true
            }),
            me
        );
        bang.position.set(collidee.point.x.toFixed(2), collidee.point.y.toFixed(2), collidee.point.z.toFixed(2));
        me.fxScene.add(bang);
        setTimeout(function(){me.fxScene.remove(bang);}, 500);

        if(collidee.object.enemy){

            me.hudMessage('Kill!', 3000);

            me.enemyDeath();

            if(me.messenger.socket)
                me.messenger.socket.emit('shot', {hit: true});
        } else {
            me.hudMessage('Miss!', 3000);
            if(me.messenger.socket)
                me.messenger.socket.emit('shot', {hit: false});
        }
    }
    else {
        console.log('no intersect');
        me.hudMessage('Miss!', 3000);
        if(me.messenger.socket)
            me.messenger.socket.emit('shot', {hit: false});
    }
    me.shootTimer = 1;
    me.controls.getPitchObject().rotation.x += DEG_22_5 * 0.3;
    me.playSound('fire');

};

Engine.prototype.enemyDeath = function() {

	var me = this;
	me.opponent.logic.exploding = true;
	me.spawnGibs(me.opponent.sprite);
	setTimeout(function() {
	        me.playSound('gore');
	}, 1000);
	setTimeout(function() {
	        me.playSound('mus-endround');
	}, 2000);

};

Engine.prototype.addBloodstain = function(x, z) {

	console.log('add bloodstain');

	var me = this;

	var size = rndInt(70,120);
	var stain = new THREE.Mesh(new THREE.PlaneGeometry(size, size, 12, 12), new me.baseMaterial({ map:me.textures.bloodstain, opacity: 0.6, transparent: true, depthTest: false }));
	stain.rotation.x = d2r(-90);
	stain.rotation.z = d2r(rndInt(0,360));
	stain.position.x = x;
	stain.position.z = z;
	me.scene.add(stain);

};

Engine.prototype.misfire = function(){
    var me = this;
    me.hudMessage('Misfire!', 3000);
    me.charging = false;
    me.shootTimer = 1;
    me.controls.speedModifier = 1;
    me.sceneTransition(function() {
        me.controls.getPitchObject().rotation.x += DEG_22_5/3;
        me.controlsObject.rotation.y += Math.random() * DEG_90 - DEG_45;
        me.hudweapon.removeClass('aim');
        me.ammoCount--;
        $('#ammo span').html(me.ammoCount);
        console.log('ammo',me.ammoCount);
        if(me.ammoCount == 0) me.startReload();
    }, 0.003);
};
