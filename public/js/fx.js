var HUDMessage = function(text, duration){
    var me = this;
    me.text = text;
    me.alpha = 1;
    me.death = Date.now() + duration;
};

HUDMessage.prototype.render = function(step, canvas, offset){
    var me = this;
    var ctx = canvas.getContext('2d');
    var tX = canvas.width / 2;
    var tY = canvas.height / 2 - offset;

    if(Date.now() > me.death){
        me.alpha -= 0.002 * step;
    }

    if (me.alpha > 0){
        ctx.font = "bold 40px Junge";
        ctx.textAlign = 'center';
        ctx.fillStyle = "rgba(0, 0, 0, " + me.alpha + ")";
        ctx.fillText(me.text, tX + 2, tY + 2);
        ctx.fillStyle = "rgba(204, 204, 0, " + me.alpha + ")";
        ctx.fillText(me.text, tX, tY);
    }

}


var SceneTransition = function(callback, coef){
    var me = this;
    me.callback = callback;
    me.initialised = false;
    me.alpha = 1;
    me.done = false;
    me.coef = coef || 0.001;
}

SceneTransition.prototype.render = function(step, canvas){
    var me = this;
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = "rgba(255, 255, 255, " + me.alpha + ")";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    if (!me.initialised){
        me.callback();
        me.initialised = true;
    }

    me.alpha -= me.coef * step;
    if (me.alpha < 0)
        me.done = true;
}


SHADERS = {
    'interfere': {
        uniforms: {
            tDiffuse: { type: "t", value: 0, texture: null },
            p: { type: "f", value: 0 },
            seed: { type: "f", value: 0 },
            amount: { type: "f", value: 0 },
            lines: { type: "f", value: 300 }
        },

        vertexShader: [
            "varying vec2 vUv;",
            "void main() {",
            "vUv = vec2( uv.x, 1.0 - uv.y );",
            "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"
        ].join("\n"),

        fragmentShader: [
            "uniform float p;",
            "uniform float amount;",
            "uniform float seed;",
            "uniform float lines;",
            "uniform sampler2D tDiffuse;",
            "varying vec2 vUv;",

            "float rand(vec2 co){",
            "    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);",
            "}",
            "void main( void ) {",
            "vec2 position = vUv;",
            "float y = -floor(lines*position.y)/lines;",
            "float disf = 0.01*(cos(position.y*130.+p*10.)+sin(position.y*183.+p*80.));",
            "float parity = 0.; if(mod(y*lines, 2.)>0.5) parity=1.; else parity=-1.;",
            "float a = smoothstep(0., 1.0, p);",
            "position.x = amount*a*(y*0.3+disf)+position.x+",
            "amount*0.5*parity*smoothstep(0.6, 0.65, p)*(sin(position.y*(12.+40.*seed))+smoothstep(0.64, 0.65, p));",
            "vec4 colorInput = texture2D( tDiffuse, position );",
            "gl_FragColor = colorInput;",
            "}"
        ].join("\n")
    }
}