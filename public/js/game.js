
var Game = function(){
    var me = this;

    if(me.detectCompat()) {

		me.engine = new Engine(function(){

		    me.engine.options = {

                sound: true,                        // Play sound?
                musvol: 0.6,                        // Music volume
                stereoscopy: false                   // Show 2 camera views for parallax viewing

            }

		    me.syncOptions();
			me.titleScene();
		});

    }
};

Game.prototype.detectCompat = function() {

		if (!window.WebGLRenderingContext) {
			// Browser has no idea what WebGL is. Suggest they
			// get a new browser by presenting the user with link to
			// http://get.webgl.org
			$('.content').html($('#detect-template').html());
			$('#detect #no-gl').show();
			return false;
		}

		var canvas = document.createElement('canvas');
		gl = canvas.getContext("webgl");
		if (!gl) {
			// Browser could not initialize WebGL. User probably needs to
			// update their drivers or get a new browser. Present a link to
			// http://get.webgl.org/troubleshooting
			$('.content').html($('#detect-template').html());
			$('#detect #fail-gl').show();
			return false;
		}

		return true;

};

Game.prototype.syncOptions = function(writeMode) {
    var me = this;
    if(writeMode === undefined) writeMode = false;

    var storage = window.localStorage;

    if (!writeMode && storage.getItem('sound') === null) {
        console.log('no localstorage options to read');
        return;
    }

    for(var k in me.engine.options) {
        var option = me.engine.options[k];

        if(writeMode) {
            console.log('save options', me.engine.options);
            storage[k] = me.engine.options[k];
        } else {
            if(storage[k] == undefined || storage[k] == null) continue;
            var v;
            switch(typeof me.engine.options[k]) {
                case 'boolean':
                    v = (storage[k] == 'true');
                    break;
                case 'number':
                    v = Number(storage[k]);
                    break;
                default:
                    v = storage[k];
            }
            me.engine.options[k] = v;
        }
    }

    console.log('storage state', storage, 'options state', me.engine.options);

};

Game.prototype.titleScene = function(){
    var me = this;

    try {
    	console.log('play music');
    	me.engine.playSound('mus-title');

    } catch(e) {
    	console.log('wait for music load');
		me.engine.sounds['mus-title'].addEventListener('canplaythrough', function() {
		    console.log('deferred play music');
			me.engine.playSound('mus-title');
		}, false);

	}

    $('.content').hide();
    $('.content').html($('#menu-template').html());
    $('.content').fadeIn(800);

    setTimeout(function() {
            $('#enemy-left, #enemy-right').addClass('in');
    }, 300);

    $(document).on('click.start', function(){
        $(document).off('click.start');
        me.engine.playSound('vox-hype');
        me.menuScene();
    });

    $(document).on('mouseover', '.menu button:not(.disabled)', function() {
            me.engine.playSound('ui-hover');
    });

    $(document).on('click', '.menu button:not(.disabled)', function() {
            me.engine.playSound('ui-click');
    });
};

Game.prototype.menuScene = function(){
    var me = this;
    $('#startprompt').fadeOut();
    $('.menu').fadeIn();
     $('.menu .actions').fadeIn();

    $('.host').on('click', function(){
        me.stagingScene();
    });

    $('.client').on('click', function(){
        $('.menu .actions').hide();
        $('.menu .join-details').show();
    });

    $('.join').on('click', function(){
        me.stagingScene($('.host-id').val());
    });

    $('.options').on('click', function(){
        me.optionsScene();
    });

    $('.howto').on('click', function(){
        me.howToScene();
    });

    $('.records').on('click', function(){
        me.recordsScene();
    });

    $('.credits').on('click', function(){
        me.creditsScene();
    });
};

Game.prototype.howToScene = function(){
    var me = this;

    $('.menu .actions').fadeOut();
    $('.menu .howtopage').fadeIn();

    $('.menu .howtopage button').on('click', function() {
            $('.menu .howtopage').fadeOut();
            me.menuScene();
    });
};

Game.prototype.recordsScene = function(){
    var me = this;
    $('#startprompt').fadeOut();
    $('.menu .actions').fadeOut();

    $('.menu .recordspage button').on('click', function() {
            $('.menu .recordspage').fadeOut();
            me.menuScene();
    });

    me.fetchRecords(function(records){
        var tmpl = _.template($('#recordstemplate').html());
        $('.menu .recordspage .records').html(tmpl({duels: records.reverse()}));
        $('.menu .recordspage').fadeIn();
    });
};

Game.prototype.optionsScene = function(){
    var me = this;

    $('.menu .actions').fadeOut();
    $('.menu .optionspage').fadeIn();

    $('#opt-sound').prop('checked', (me.engine.options.sound));
    $('#opt-musvol').val(me.engine.options.musvol * 10);
    $('#opt-stereoscopy').prop('checked', (me.engine.options.stereoscopy));

    $('#optionsform input, #optionsform select').off();

    $('#opt-sound').on('change', function() {
         me.engine.options.sound = $(this).is(':checked');
         if(!me.engine.options.sound) {
             me.engine.sounds['mus-title'].volume = 0;
         } else {
             me.engine.sounds['mus-title'].volume = me.engine.options.musvol;
             if( me.engine.sounds['mus-title'].paused ) me.engine.sounds['mus-title'].play();
         }
         me.syncOptions(true);
    });
    $('#opt-musvol').on('change', function() {
         me.engine.options.musvol = $(this).val() / $(this).attr('max');
         me.engine.sounds['mus-title'].volume = me.engine.options.musvol;
         me.syncOptions(true);
    });
    $('#opt-stereoscopy').on('change', function() {
         me.engine.options.stereoscopy = $(this).is(':checked');
         me.syncOptions(true);
    });

    $('.menu .optionspage button').on('click', function() {
            $('.menu .optionspage').fadeOut();
            me.menuScene();
    });
};

Game.prototype.creditsScene = function(){
    var me = this;
    $('#startprompt').fadeOut();
    $('.menu .actions').fadeOut();
    $('.menu .creditspage').fadeIn();

    $('.menu .creditspage button').on('click', function() {
            $('.menu .creditspage').fadeOut();
            me.menuScene();
    });
};

Game.prototype.stagingScene = function(uuid)
{
    var me = this;

    me.messenger = new Messenger();

    if(!window.hasOwnProperty('io'))
    {
        console.log("WARNING: Running statically. Entering game mode without networking.");
        me.gameplayScene();
    }
    else
    {
        me.sharedState = new SharedState(me.messenger.socket);
    }
    if(uuid)
    {
        me.messenger.start({
            uuid: uuid,
            onConnect: function(data){
                if(data.error)
                {
                    $('.get-ready').hide();
                    $('.challenger-name').hide();
                    $('.uuid').val(data.error);
                    setTimeout(function(){window.location.reload();}, 3000);
                }
                $('.menu').html($('#staging-template').html());
                $('.no-opponent').hide();
                $('.ready').on('click', function(){
                    var playerName = $('#player-name').removeClass('error').val();
                    if(!playerName){
                        $('#player-name').addClass('error');
                        return;
                    }
                    $('.challenger-name').hide();
                    $('.no-opponent').show();
                    $(this).replaceWith('<h2>I\'m Ready</h2>');
                    function challengerReady(){
                        me.sharedState.removeRefreshListener(challengerReady);
                        me.sharedState.set('challengerName', playerName);
                        me.messenger.ready();
                    };
                    me.sharedState.addRefreshListener(challengerReady);
                    me.sharedState.refresh();
                });
            },
            onReady: me.gameplayScene.bind(me)
        });
    }
    else
    {
        me.messenger.start({
            uuid: null,
            onConnect: me.hostOptionsScene.bind(me),
            onChallenger: function(){
                $('.get-ready').show();
                $('.no-opponent').hide();
                $('.uuid').hide();
                $('.uuid-help').hide();
            },
            onReady: me.gameplayScene.bind(me)
        });
    }
};

Game.prototype.hostOptionsScene = function(){
    var me = this;
    $('.menu').html($('#setup-template').html());
    $('.setup-complete').on('click', function()
    {
        var playerName = $('#player-name').removeClass('error').val();
        var reason = $('#reason').removeClass('error').val();
        var rounds = $('#num-rounds').val();
        if(!playerName){
            $('#player-name').addClass('error');
            return;
        }
        if(!reason){
            $('#reason').addClass('error');
            return;
        }
        $('.menu').html($('#staging-template').html());
        $('.challenger-name').hide();
        $('.get-ready').hide();
        $('.uuid-help').show();
        $('.uuid').val(me.messenger.identifier);

        $('.ready').on('click', function(){
            $('.no-opponent').show();
            $(this).replaceWith('<h2>I\'m Ready</h2>');
            function hostReady(){
                me.sharedState.removeRefreshListener(hostReady);
                me.sharedState.submitDelta({
                    hostName: playerName,
                    duelReason: reason,
                    numRounds: rounds,
                    currentRound: 1,
                    hostScore: 0,
                    challengerScore: 0
                });
                me.messenger.ready();
            };
            me.sharedState.addRefreshListener(hostReady);
            me.sharedState.refresh();
        });
    });
}

Game.prototype.gameplayScene = function(){
    var me = this;
    me.engine.sounds['mus-title'].pause();
    $('.content').html($('#gameplay-template').html());
    if(me.sharedState){
        $('#hud-name-1 .name').html(me.sharedState.get('hostName'));
        $('#hud-name-2 .name').html(me.sharedState.get('challengerName'));
        $('#current-round').html(me.sharedState.get('currentRound'));
        $('#num-rounds').html(me.sharedState.get('numRounds'));
    }
    me.engine.start(me.messenger, me.sharedState);

    //me.addDatGUI();
};

Game.prototype.addDatGUI = function() {

	var me = this;

	var gui = new dat.GUI({ autoPlace: false });

	var customContainer = document.getElementById('my-gui-container');
	customContainer.appendChild(gui.domElement);

	var f1 = gui.addFolder('Ambient Color');
	f1.add(me.engine.ambient.color, 'r', 0, 1);
	f1.add(me.engine.ambient.color, 'g', 0, 1);
	f1.add(me.engine.ambient.color, 'b', 0, 1);

	var f1a = gui.addFolder('Fog');
	f1.add(me.engine.scene.fog.color, 'r', 0, 1);
	f1.add(me.engine.scene.fog.color, 'g', 0, 1);
	f1.add(me.engine.scene.fog.color, 'b', 0, 1);

	var f2 = gui.addFolder('Player');
	f2.add(me.engine.playerState, 'state').listen();

	var f3 = gui.addFolder('Enemy');
	f3.add(me.engine.opponent.logic, 'state').listen();
	f3.add(me.engine.opponent, 'angle_id').listen();

};

Game.prototype.fetchRecords = function(callback){
    var me = this;
    $.ajax({
        url: '/api/records',
        dataType: 'json',
        success: function(response){
            for(var idx = 0; idx < response.records.length; idx++){
                response.records[idx].date = moment(response.records[idx].date);
            }
            callback(response.records.splice(0, 10));
        }
    });
};


var g = new Game();
