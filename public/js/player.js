const DEG_360 = Math.PI*2;
const DEG_180 = Math.PI;
const DEG_90 = Math.PI*0.5;
const DEG_45 = Math.PI*0.25;
const DEG_22_5 = Math.PI*0.125;

var StateTracker = function(owner) {
	var me = this;

	me.owner = owner;

	me.state = 'stationary';

	me._states = {

    	stationary: function() {},
    	moving: function() {},
    	shooting: function() {},
    	needreload: function() {},
    	reloading: function() {},
    	exploding: function() {}

    };


};

StateTracker.prototype.setState = function(stateName) {

	var me = this;
	if (!stateName in me._states) throw 'Invalid player state name ' + stateName;
	if(me.state != stateName) {
		me._states[stateName].call(me);
		me.state = stateName;
		me.owner.framenum = 0;
	}
};

StateTracker.prototype.getState = function() {

	var me = this;
	return me.state;

};

StateTracker.prototype.is = function(stateName) {

	var me = this;
	return (me.state == stateName);

};

/**
    ANIMATION DEFINITIONS

    - Sprites are in /sprites/anims
    - format of filenames is <prefix>_<anim/sprite>_<angle_id>_<frame_num>.png
    - prefix: folder name
    - anim/sprite: name of anim e.g. walk
    - angle: derived from camera angle in Enemy.setSpriteImage
        - a: front (from viewer POV)
        - b: facing front-left (right side visible)
        - c: facing left (right side visible)
        - d: facing away-left
        - e: facing away
        - f: mirrored from d
        - g: mirrored from c
        - h: mirrored from b

**/

var enemy_anims = {
	prefix: 'shooter',
	timing: 10,
	angles: ['a','b','c','d','e','f','g','h'],
	anims: {
		stationary: {
			sprite: 'walk',
			frames: 1
		},
		moving: {
			sprite: 'walk',
			frames: 4
		},
		shooting: {
			sprite: 'fire',
			frames: 1
		},
		exploding: {
		    sprite: 'bloodsplat',
		    frames: 4,
		    noangles: true,
		    loop: false,
		    onComplete: function() {
		        if(this.hasExploded) return;
		        console.log('explode complete', this);
		        this.sprite.visible = false;
		        this.sprite._shadowMesh.visible = false;
		        this.hasExploded = true;
		        this.opts.engine.addBloodstain(this.sprite.position.x, this.sprite.position.z);
		    }
		}
	},
    hitWidths: ['30', '25', '20', '25', '30', '25', '20', '25'],
    hitHeights: ['85', '85', '85', '85', '85', '85', '85', '85']
};

var Enemy = function(opts){

    var me = this;
    me.opts = opts;

    me.logic = new StateTracker(me);

    me.animdefs = enemy_anims;
    me.currentAnim = 'moving';
    me.currentImage = null;
    me.framenum = 0;
    me.frametimer = 0;
    me.angle_id = 0;

    me.gameWorldRotation = 0;

    me.sprite = new Billboard(
        new THREE.PlaneGeometry(100, 100, 10, 10),
        new THREE.MeshBasicMaterial({
            map: opts.engine.textures.player,
            transparent: true
        })
    );

    me.sprite.enemy = true;
    me.sprite.model = me;

    me.hasExploded = false;

    me.addToScene();
};

Enemy.prototype.addToScene = function() {

	var me = this;
	me.opts.engine.scene.add(me.sprite);
    me.sprite.addShadow(50, me.opts.engine);
    me.sprite.rotation.y = d2r(-90);
    me.sprite.position.y = 50;
    me.sprite.position.x = 0;
    me.sprite.position.z = -200;

};

Enemy.prototype.animate = function() {
	var me = this;

	me.setSpriteImage();

	me.frametimer++;
	if(me.frametimer >= me.animdefs.timing) {
		me.framenum++;
		me.frametimer = 0;

        if (me.framenum >= me.animdefs.anims[me.currentAnim].frames) {
            if(me.animdefs.anims[me.currentAnim].loop !== false) {
                me.framenum = 0;
            } else {
                if(typeof me.animdefs.anims[me.currentAnim].onComplete == 'function') {
                    me.animdefs.anims[me.currentAnim].onComplete.call(me);
                }
            }
        }

	}
};

Enemy.prototype.setSpriteImage = function() {
	var me = this;

    me.currentAnim = me.logic.exploding ? 'exploding' : me.logic.getState();

    if(! me.animdefs.anims[me.currentAnim].noangles) {

        var angToCam = Math.atan2(me.opts.engine.controlsObject.position.z - me.sprite.position.z, me.opts.engine.controlsObject.position.x - me.sprite.position.x);
        var adjAngle = angToCam + me.sprite.rotation.y;
        while(adjAngle > DEG_360){
            adjAngle -= DEG_360;
        }
        while(adjAngle < 0) {
            adjAngle += DEG_360;
        }
        me.angle_id = 6 - Math.floor((adjAngle + 3.14159265 - 0.3926990815) / 0.785398163);	// 180, 22.5 and 45 degrees in radians
        if (me.angle_id < 0) me.angle_id += 8;

        var skey = me.animdefs.prefix + '_' + me.animdefs.anims[me.currentAnim].sprite + '_' + me.animdefs.angles[me.angle_id] + '_' + (me.framenum + 1);

    } else {

        var skey = me.animdefs.prefix + '_' + me.animdefs.anims[me.currentAnim].sprite + '_' + (me.framenum + 1);

	}

	me.sprite.material.map = me.currentMap = me.opts.engine.textures[skey];
	me.sprite.material.needsUpdate = true;

    me.hitWidth = me.animdefs.hitWidths[me.angle_id];
    me.hitHeight = me.animdefs.hitHeights[me.angle_id];
};

Enemy.prototype.update = function(data){
    var me = this;
    me.sprite.position.x = data.x;
    me.sprite.position.y = data.y + 36;
    me.sprite.position.z = data.z;
    me.sprite.rotation.y = data.rotation ;
    me.logic.setState(data.state);
 }


var Messenger = function(subject){
    var me = this;
    me.subject = subject;
    try{
        me.socket = io.connect(window.location.origin);
    }
    catch(err){
        me.socket = null;
        console.log("ERROR: Could not obtain socket.");
    }
};

Messenger.prototype.start = function(opts){
    var me = this;
    if(me.socket)
    {
        if(opts.uuid)
        {
            me.socket.emit('init', {id: opts.uuid});
        }
        else
        {
            me.socket.emit('init');
            me.socket.on('challenger', opts.onChallenger);
        }
        me.socket.on('connected', function(data)
        {
            opts.onConnect(data);
            me.onConnect(data);
        });
        me.socket.on('ready', function(data){
            opts.onReady(data);
            me.onReady(data);
        });
    }
};

Messenger.prototype.onConnect = function(data){
    var me = this;
    me.identifier = data.identifier;
    me.participant = data.participant;
};

Messenger.prototype.onReady = function(data){
    var me = this;
    if(me.socket)
        me.socket.on('update', me.onUpdate.bind(me));
};

Messenger.prototype.onUpdate = function(data){
    var me = this;
    if(me.subject)
        me.subject.update(data);
};

Messenger.prototype.ready = function()
{
    var me = this;
    me.socket.emit('ready');
};

Messenger.prototype.update = function(data){
    var me = this;
    data.identifier = me.identifier;
    data.participant = me.participant;
    if(me.socket)
        me.socket.emit('update', data);
};

Messenger.prototype.control = function(data){
    var me = this;
    if(me.socket)
        me.socket.emit('control', data);
};

var SharedState = function(socket){
    var me = this;
    me.socket = socket;
    me.deltaListeners = [];
    me.refreshListeners = [];
    me.values = {};
    me.socket.on('fullState', me.handleRefreshedState.bind(me));
    me.socket.on('stateChanged', me.handleStateChanged.bind(me));
    me.refresh();
};

SharedState.prototype.refresh = function(){
    var me = this;
    me.socket.emit('refreshState');
};

SharedState.prototype.addDeltaListener = function(callback){
    var me = this;
    if (me.deltaListeners.indexOf(callback) == -1){
        me.deltaListeners.push(callback);
    }
};

SharedState.prototype.removeDeltaListener = function(callback){
    var me = this;
    if (me.deltaListeners.indexOf(callback) > -1){
        delete me.deltaListeners[me.deltaListeners.indexOf(callback)]
    }
};

SharedState.prototype.addRefreshListener = function(callback){
    var me = this;
    if (me.refreshListeners.indexOf(callback) == -1){
        me.refreshListeners.push(callback);
    }
};

SharedState.prototype.removeRefreshListener = function(callback){
    var me = this;
    if (me.refreshListeners.indexOf(callback) > -1){
        delete me.refreshListeners[me.refreshListeners.indexOf(callback)]
    }
};

SharedState.prototype.handleRefreshedState = function(state){
    var me = this;
    me.values = state;
    for(idx in me.refreshListeners){
        me.refreshListeners[idx](me.values);
    }
};

SharedState.prototype.handleStateChanged = function(data){
    var me = this;
    if(data.delta){
        for (key in data.delta){
            me.values[key] = data.delta[key];
        }
    }
    if(data.removed){
        for (key in data.removed){
            delete me.values[key];
        }
    }
    for(idx in me.deltaListeners){
        me.deltaListeners[idx](data.delta, data.added, data.removed);
    }
}

SharedState.prototype.submitDelta = function(delta, remove)
{
    var me = this;
    me.socket.emit('stateDelta', {
        delta: delta,
        remove: remove
    });
}

SharedState.prototype.get = function(key){
    var me = this;
    if (me.values.hasOwnProperty(key)){
        return me.values[key];
    } else {
        return null;
    }
}

SharedState.prototype.set = function(key, value){
    var me = this;
    var delta = {};
    delta[key] = value;
    me.submitDelta(delta);
}
