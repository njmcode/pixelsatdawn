// utils
var d2r = function(degs) {

    var rads = (Math.PI / 180) * degs;
    return rads;

};

var rndInt = function(from, to) {

    return Math.floor(Math.random() * (to - from + 1) + from);

};

