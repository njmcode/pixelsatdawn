/**
 * @author mrdoob / http://mrdoob.com/
 * extended by njmcode & thegrieve
 */

THREE.PointerLockControls = function ( camera, camera2 ) {

	var scope = this;

	scope.speedModifier = 1.5;

	var pitchObject = new THREE.Object3D();
	pitchObject.add( camera );
	if(camera2) {
	    pitchObject.add(camera2);
	}

	var yawObject = new THREE.Object3D();
	yawObject.position.y = 10;
	yawObject.add( pitchObject );

	var moveForward = false;
	var moveBackward = false;
	var moveLeft = false;
	var moveRight = false;

	var isOnObject = false;
	var allowJump = false;
	var canJump = false;

	var yawClamped = false;
	var maxYaw = 0;
	var minYaw = 0;

	var pitchClamped = false;
	var maxPitch = 0;
	var minPitch = 0;

	var forwardOnly = false;

	var velocity = new THREE.Vector3();

	var PI_2 = Math.PI / 2;

	var onMouseMove = function ( event ) {

		if ( scope.enabled === false ) return;

		var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
		var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

		yawObject.rotation.y -= movementX * 0.002;
		pitchObject.rotation.x -= movementY * 0.002;

		if(yawClamped){
			yawObject.rotation.y = yawObject.rotation.y > maxYaw ? maxYaw : yawObject.rotation.y;
			yawObject.rotation.y = yawObject.rotation.y < minYaw ? minYaw : yawObject.rotation.y;
		}

		if(pitchClamped){
			pitchObject.rotation.x = pitchObject.rotation.x > maxPitch ? maxPitch : pitchObject.rotation.x;
			pitchObject.rotation.x = pitchObject.rotation.x < minPitch ? minPitch : pitchObject.rotation.x;
		}

		pitchObject.rotation.x = Math.max( - PI_2, Math.min( PI_2, pitchObject.rotation.x ) );

	};

	this.isMoving = function() {

		//console.log('isMoving', (moveForward || moveBackward || moveLeft || moveRight));
		return (moveForward || moveBackward || moveLeft || moveRight);

	};

	this.getVelocity = function() {

		return (Math.abs(velocity.x) + Math.abs(velocity.y) + Math.abs(velocity.z)) / 3;

	};

	var onKeyDown = function ( event ) {

		switch ( event.keyCode ) {

			case 38: // up
			case 87: // w
				moveForward = true;
				break;

			case 37: // left
			case 65: // a
				moveLeft = true; break;

			case 40: // down
			case 83: // s
				moveBackward = true;
				break;

			case 39: // right
			case 68: // d
				moveRight = true;
				break;

			case 32: // space
				if ( allowJump === true && canJump === true ) velocity.y += 10;
				canJump = false;
				break;

		}

	};

	var onKeyUp = function ( event ) {

		switch( event.keyCode ) {

			case 38: // up
			case 87: // w
				moveForward = false;
				break;

			case 37: // left
			case 65: // a
				moveLeft = false;
				break;

			case 40: // down
			case 83: // a
				moveBackward = false;
				break;

			case 39: // right
			case 68: // d
				moveRight = false;
				break;

		}

	};

	document.addEventListener( 'mousemove', onMouseMove, false );
	document.addEventListener( 'keydown', onKeyDown, false );
	document.addEventListener( 'keyup', onKeyUp, false );

	this.enabled = false;

	this.getObject = function () {

		return yawObject;

	};

	this.getPitchObject = function () {
		return pitchObject;
	};

	this.isOnObject = function ( boolean ) {

		isOnObject = boolean;
		canJump = boolean;

	};

	this.update = function ( delta ) {

		if ( scope.enabled === false ) return;
		delta *= 0.1;

		velocity.x += ( - velocity.x ) * 0.1;
		velocity.z += ( - velocity.z ) * 0.1;

		velocity.y -= 0.25;

		if ( moveForward ) velocity.z -= 0.12 * scope.speedModifier;
		if ( moveBackward && !forwardOnly) velocity.z += 0.12 * scope.speedModifier;

		if ( moveLeft && !forwardOnly) velocity.x -= 0.12 * scope.speedModifier;
		if ( moveRight && !forwardOnly) velocity.x += 0.12 * scope.speedModifier;

		if ( isOnObject === true ) {

			velocity.y = Math.max( 0, velocity.y );

		}

		yawObject.translateX( velocity.x * delta);
		yawObject.translateY( velocity.y * delta);
		yawObject.translateZ( velocity.z * delta);

		if ( yawObject.position.y < 10 ) {

			velocity.y = 0;
			yawObject.position.y = 10;

			canJump = true;

		}

	};

	this.getDirection = function() {

		// assumes the camera itself is not rotated

		var direction = new THREE.Vector3( 0, 0, -1 );
		var rotation = new THREE.Euler( 0, 0, 0, "YXZ" );

		return function() {

			var v = yawObject.position.clone();

			rotation.set( pitchObject.rotation.x, yawObject.rotation.y, 0 );

			v.copy( direction ).applyEuler( rotation );

			return v;

		}();

	};

	this.clampYaw = function(range){
		yawClamped = true;
		maxYaw = yawObject.rotation.y + range;
		minYaw = yawObject.rotation.y - range;
	};

	this.unclampYaw = function(range){
		yawClamped = false;
	};

	this.clampPitch = function(range){
		pitchClamped = true;
		maxPitch = pitchObject.rotation.y + range;
		minPitch = pitchObject.rotation.y - range;
	};

	this.unclampPitch = function(range){
		pitchClamped = false;
	};

	this.setForwardOnly = function(state){
		forwardOnly = state;
	}

};
