var SharedState = function(){
    var me = this;
    me.sockets = [];
    me.clear();
}

SharedState.prototype.addSocket = function(socket){
    var me = this;
    if (me.sockets.indexOf(socket) == -1)
    {
        me.sockets.push(socket);
        socket.on('stateDelta', me.handleDelta.bind(me));
        socket.on('refreshState', me.refreshState.bind(me, socket));
    }
}

SharedState.prototype.handleDelta = function(data){
    var me = this;
    if(data.delta)
    {
        me.update(data.delta);
    }
    if(data.remove)
    {
        me.remove(data.remove);
    }
};

SharedState.prototype.refreshState = function(socket){
    var me = this;
    socket.emit('fullState', me.values);
};

SharedState.prototype.clear = function()
{
    var me = this;
    me.values = {};
    me.keys = [];
};

SharedState.prototype.update = function(obj){
    var me = this;
    var added = [];
    var delta = {};
    for(key in obj){
        if(me.keys.indexOf(key) > -1)
        {
            me.keys.push(key);
            added.push(key);
        }
        if(me.values[key] != obj[key])
        {
            me.values[key] = obj[key];
            delta[key] = obj[key];
        }
    }
    for(var idx in me.sockets)
    {
        me.sockets[idx].emit('stateChanged', {
            delta: delta,
            added: added,
            removed: []
        })
    }
    console.log("DELTA:");
    console.log(delta);
};

SharedState.prototype.remove = function(keys){
    var me = this;
    var deleted = [];
    for(key in obj){
        if(me.keys.indexOf(key) > -1)
        {
            delete me.values[key];
            delete me.keys[me.keys.indexOf(key)];
            deleted.push(key);
        }
    }
    for(var idx in me.sockets)
    {
        me.sockets[idx].emit('stateChanged', {
            delta: {},
            added: [],
            removed: deleted
        })
    }
};

module.exports = SharedState;
