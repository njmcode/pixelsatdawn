var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var duel = require('./duel.js');
var state =require('./state.js');
var recordsAPI = require('./records_api.js');


var server = express();
var ejs = require('ejs');
ejs.open = "{{";
ejs.close = "}}";
server.engine('.html', ejs.__express);
server.set('views', __dirname + '/tmpl');
server.set('view engine', 'html');

server.use(express.static(__dirname + '/public'));

server.get('/', function(req, res)
    {
        res.render('index', {
            cache_buster: Math.round(Math.random() * 100000000)
        });
    }
);

server.get('/test/sockets', function(req, res)
    {
        res.render('socket_test', {
            cache_buster: Math.round(Math.random() * 100000000)
        });
    }
);

server.get('/api/records', recordsAPI.getRecords);

var db = mongoose.connect('mongodb://pixels:dawn@paulo.mongohq.com:10077/pixelsatdawn');

var io = require('socket.io').listen(server.listen(process.env.PORT || 8081), {log: false});

var states = {};

io.sockets.on('connection', function(socket)
    {
        console.log("New player");
        console.log(socket.handshake.address);
        var d = new duel(socket, states, db);
    }
)
