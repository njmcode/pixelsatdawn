var Record = require('./record.js');

module.exports = RecordsAPI = {

	getRecords: function(req, res){
		res.writeHead(200, {'Content-Type': 'application/json'});
		var output = {records: []};
		Record.find({})
		  .sort({'date': -1})
		  .limit(10)
		  .execFind(function(error, records){
			records.map(function(rec){
				output.records.push(rec);
			});
			res.write(JSON.stringify(output));
			res.send();
		});
	}

};
