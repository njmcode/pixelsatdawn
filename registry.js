var Registry = function()
{
    var me = this;
    me.children = {};
}

Registry.prototype.add = function(name, child)
{
    var me = this;
    me.children[name] = child;
};

Registry.prototype.get = function(name)
{
    var me = this;
    return me.children[name];
}

module.exports = new Registry();