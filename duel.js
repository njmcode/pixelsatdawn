var uuid = require('node-uuid');
var state = require('./state.js');
var registry = require('./registry.js');
var Record = require('./record.js');

var Duel = function(socket, states)
{
    var me = this;
    me.states = states;
    me.socket = socket;
    me.socket.on('init', me.init.bind(me));
};


Duel.prototype.getName = function(){
    var me = this;
    if(!me.state) return "UNKNOWN";
    if(!me.initialized) return "UNKNOWN";
    if(me.participant == 0) return me.state.values.hostName;	
    if(me.participant == 1) return me.state.values.challengerName;	
}

Duel.prototype.init = function(data)
{
    var me = this;
    if(data && data.id)
    {
        if(registry.get(data.id)){
            me.identifier = data.id;
            me.initialized = true;
            me.participant = 1;
            me.setOpponent(registry.get(me.identifier));
            me.opponent.setOpponent(me);
            me.opponent.socket.emit('challenger');
        }
        else
        {
            me.socket.emit('connected', {error: 'Opponent not found'});
            return;
        }
    }
    else
    {
        me.identifier = uuid.v4();
        me.initialized = true;
        me.participant = 0;
        registry.add(me.identifier, me);
        me.states[me.identifier] = new state();
    }
    me.state = me.states[me.identifier];
    me.state.addSocket(me.socket);

    me.socket.emit('connected', {identifier: me.identifier, participant: me.participant});
    me.socket.on('ready', me.onReady.bind(me));
    me.socket.on('reset', me.onReset.bind(me));
}

Duel.prototype.emitAll = function(){
    var me = this;
    me.socket.emit.apply(me.socket, arguments);
    me.opponent.socket.emit.apply(me.opponent.socket, arguments);
}

Duel.prototype.onReady = function()
{
    var me = this;
    console.log(me.getName() + " - onReady");
    me.ready = true;
    if(me.opponent.ready)
    {
        console.log('READY!');
        me.emitAll('ready');
    }
};

Duel.prototype.onReset = function()
{
    var me = this;
    console.log(me.getName() + " - onReset");
    clearTimeout(me.phaseTimeout);
    me.reset = true;
    if(me.opponent.reset){
        console.log('RESET!');
        me.reset = me.opponent.reset = false;
        me.emitAll('roundStart');
        me.emitAll('countdown', 11);
        me.phaseTimeout = setTimeout(function(){
            me.walkPhase();
        }, 11000);
    }
}

Duel.prototype.walkPhase = function()
{
    var me = this;
    console.log(me.getName() + " - WALK");
    clearTimeout(me.phaseTimeout);
    me.shotState = null;
    me.emitAll('walk');
    me.phaseTimeout = setTimeout(function(){
        me.firePhase();
        me.opponent.firePhase();
    }, 8000 + Math.floor(Math.random()*5000));
}

Duel.prototype.firePhase = function()
{
    var me = this;
    console.log(me.getName() + " - FIRE");
    clearTimeout(me.phaseTimeout);
    me.socket.on('shot', function(data){
        me.shotState = data.hit;
        var delta = {};
        if(me.shotState === true){
            if (me.opponent.shotState === true) {
                delta.currentRound = me.state.values.currentRound + 1;
                me.emitAll('outcome', {win: "draw"});
                me.shotState = me.opponent.shotState = null;
                me.socket.removeAllListeners('shot');
                me.opponent.socket.removeAllListeners('shot');
            } else {
                delta.currentRound = me.state.values.currentRound + 1;
                if(me.participant === 0){
                    delta.hostScore = me.state.values.hostScore + 1;
                } else {
                    delta.challengerScore = me.state.values.challengerScore + 1;
                }
                me.socket.emit('outcome', {win: "win"});
                me.opponent.socket.emit('outcome', {win: "lose"});
                me.shotState = me.opponent.shotState = null;
                me.socket.removeAllListeners('shot');
                me.opponent.socket.removeAllListeners('shot');
            }
        } else if (me.opponent.shotState === false) {
            me.emitAll('outcome', {win: "none"});
            me.shotState = me.opponent.shotState = null;
        }
        me.state.update(delta);
        if(me.state.values.currentRound > me.state.values.numRounds)
        {
            var data = {
                hostScore: me.state.values.hostScore,
                challengerScore: me.state.values.challengerScore,
            };
            if (data.hostScore > data.challengerScore){
                data.winner = 0;
            } else if (data.hostScore < data.challengerScore){
                data.winner = 1;
            } else {
                data.winner = 2;
            }
            me.emitAll('finish', data);
            console.log("GameOver");
            console.log(data);
            var record = new Record();
            record.hostName = me.state.values.hostName;
            record.challengerName = me.state.values.challengerName;
            record.hostScore = data.hostScore;
            record.challengerScore = data.challengerScore;
            record.duelReason = me.state.values.duelReason;
            record.save();
        }
    });
    console.log('FIRE!');
    me.socket.emit('fire');

}

Duel.prototype.setOpponent = function(opponent)
{
    var me = this;
    console.log(me.getName() + " - setOpponent");
    me.opponent = opponent;
    me.socket.on('update', me.opponent.handleUpdate.bind(me.opponent));
}

Duel.prototype.handleUpdate = function(data)
{
    var me = this;
    if (!me.initialized)
        return;

    var dirty = false;
    for (var key in data){
        if(!me.oldData || !me.oldData.hasOwnProperty(key) || me.oldData[key] != data[key])
        {
            dirty = true;
        }
    }

    if (dirty){
        me.socket.emit('update', data);
        me.oldData = data;
    }
}


module.exports = Duel;
